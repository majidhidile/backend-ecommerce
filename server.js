const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors'); // Import the cors package
const app = express();
const PORT = 2121;
const jwt = require('jsonwebtoken');

const admin = require('firebase-admin');
const serviceAccount = require('./lilac.json');
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});


// Connect to MongoDB
mongoose.connect('mongodb+srv://majid:majid@lilac.9ko8nno.mongodb.net/test?retryWrites=true&w=majority', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

// Use the cors middleware
app.use(cors());

// Define User schema
const userSchema = new mongoose.Schema({
  name: String,
  email: String,
  password: String,
});

const User = mongoose.model('User', userSchema);

// Middleware to parse JSON
app.use(express.json());

//REGISTER API
app.post('/api/register', async (req, res) => {
    try {
      const { name, email, password } = req.body;

      // Check if the email already exists in the database
      const existingUser = await User.findOne({ email });
      if (existingUser) {
        return res.status(409).json({ error: 'Email already registered' });
      }

      // Create a new user instance
      const newUser = new User({ name, email, password });

      // Save the user to the database
      await newUser.save();

      // Generate a JWT token
      const token = jwt.sign({ email }, 'majid.js');
      let userData={}
      userData.name=name,
      userData.email= email
      userData.password= password
      userData.token= token


      res.json({ message: 'User registered successfully', userData });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Failed to register user' });
    }
  });

//LOGIn API

app.post('/api/login', async (req, res) => {
    try {
      const { email, password } = req.body;

      // Check if the email exists in the database
      let user = await User.findOne({ email });
      if (!user) {
        return res.status(404).json({ error: 'User not found' });
      }

      // Perform password validation
      const isValidPassword = user.password === password
      if (!isValidPassword) {
        return res.status(401).json({ error: 'Invalid password' });
      }

      // Generate a JWT token
      const token = jwt.sign({ email }, 'majid.js');
      let userData={}

      userData.name=user.name
      userData.email=user.email
      userData.token=token

      // Return the token and user details in the response
      res.json({ message: 'User Logination successfully', userData });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Failed to log in' });
    }
  });

  app.post('/api/googleAuth', async (req, res) => {
    try {
      const { token } = req.body;
      console.log("HERE-->1",token)

      // Verify the Firebase ID token
      const decodedToken = await admin.auth().verifyIdToken(token);
      const { email } = decodedToken;
      console.log("HERE-->2",email)
      // Check if the email already exists in the database
      let user = await User.findOne({ email });

      console.log("HERE-->3",user)

      if (!user) {
        // If the user does not exist, create a new user instance
        user = new User({ email });
        await user.save();
      }

      // Generate a JWT token
      const authToken = jwt.sign({ email }, 'majid.js');
      let userData={}
      userData.token=authToken
      userData.name=user.name
      userData.email=user.email


      // Return the token and user details in the response
      res.json({ message: 'Successfully Completed', userData });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Failed to authenticate with Google' });
    }
  });

// Start the server
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
